
<?php
/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 15.02.2017
 * Time: 12:03
 */
 ?>
<div class="row">
    <div class="large-9 columns">
        <h1>Catégories</h1>
        <hr>
    </div>
</div>
<div class="row large-up-3">
<?php
foreach ($page['categories'] as $category) {
?>
    <div class="column">
        <div class="card">
            <div class="card-section">
                <h4>
                    <a href="<?php echo site_url('CCategory/view/' . $category->getId()); ?>">
                        <?php echo $category->getLabel(); ?>
                    </a>
                </h4>
                <ul>
                    <?php
                    foreach ($category->getChildren() as $child) {
                        echo '<li><a href="' . site_url('CCategory/view/' . $child->getId()) . '">' . $child->getLabel() . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

<?php
}
?>
</div>