<div class="row">
    <div class="large-9 columns">
        <h1>Bienvenue sur Aurora</h1>
        <p class="lead">La plateforme de MOOC qui vous veut du bien !</p>
        <hr>
    </div>
</div>
<div class="row">
    <div class="large-9 columns">
        <h4>Catégories disponibles</h4>
    </div>
</div>
<div class="row large-up-3">
<?php
foreach ($page['categories'] as $category) {
?>
    <div class="column">
        <div class="card">
            <div class="card-section">
                <h4>
                    <a href="<?php echo site_url('CCategory/view/' . $category->getId()); ?>">
                        <?php echo $category->getLabel(); ?>
                    </a>
                </h4>
                <ul>
                    <?php
                    foreach ($category->getChildren() as $child) {
                        echo '<li><a href="' . site_url('CCategory/view/' . $child->getId()) . '">' . $child->getLabel() . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

<?php
}
?>
</div>