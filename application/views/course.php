<?php $course = $page['course']; ?>
<div class="row large-up-12">
    <div class="column">
        <article>
            <h1><?php echo $course->getTitle(); ?> </h1>
            <h3 class="subheader"><?php echo $course->getDescription(); ?></h3>
            <hr>
            <div>
                <?php echo $course->getContent(); ?>
            </div>
            <?php
            if(isset($_SESSION['token'])){
                ?>
                <a href="<?php echo site_url().'/CCourse/edit_form/'.$course->getId();?>">Modifier</a>
            <?php }
            ?>
        </article>
    </div>
</div>
