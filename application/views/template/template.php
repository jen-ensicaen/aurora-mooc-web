<?php
/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 07.02.2017
 * Time: 12:09
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Aurora MOOC</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/foundation.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/foundation-icons.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css">
</head>

<body>
<?php
$this->load->view("template/header");
?>
<div class="off-canvas-content" data-off-canvas-content>
    <?php
        if(isset($page['location']))
            $this->load->view($page['location']);
    ?>
</div>
<script src="<?php echo base_url(); ?>js/vendor/jquery.js"></script>
<script src="<?php echo base_url(); ?>js/vendor/what-input.js"></script>
<script src="<?php echo base_url(); ?>js/vendor/foundation.js"></script>
<script src="<?php echo base_url(); ?>js/app.js"></script>
</body>
</html>