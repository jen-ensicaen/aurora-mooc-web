<?php
/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 07.02.2017
 * Time: 12:08
 */
?>
<div class="top-bar">
    <div class="top-bar-title">
        <span data-open="offCanvasLeft" data-hide-for="never">
            <button class="menu-icon dark" type="button" data-toggle></button>
        </span>
        <strong><?php echo $this->config->item('site_name'); ?></strong>
    </div>
    <div id="responsive-menu">
        <div class="top-bar-right">
            <ul class="menu">
                <?php
                if(!isset($_SESSION['token'])){
                ?>
                <li><a href="<?php echo site_url().'/CLogin' ;?>">Connexion</a></li>
                <li><a href="<?php echo site_url().'/CLogin/register_form' ;?>">Inscription</a></li>
                <?php }
                else{ ?>
                    <li><a href="<?php echo site_url().'/CLogin/disconnect' ;?>">Se déconnecter</a></li>
                <?php   }
                ?>
                <li class="divider"></li>
                <li><input type="search" placeholder="Recherche..."></li>
                <li><button type="button" class="button">Rechercher</button></li>
            </ul>
        </div>
    </div>
</div>

<?php $this->load->view('menu/leftmenu'); ?>
