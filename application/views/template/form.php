
<div class="row">
    <div class="large-offset-3 large-6 columns">
        <?php
        if(isset($_SESSION['token']))
        {
            echo createForm($page['form']);
        }
        else
        {
            redirect(site_url());
        }?>
    </div>
</div>