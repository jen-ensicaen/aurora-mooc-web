<?php
/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 13.02.2017
 * Time: 11:19
 */
?>

<aside class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    <ul class="menu vertical">
        <li class="menu-text"><?php echo $this->config->item('site_name'); ?></li>
        <li><a href="<?php echo base_url(); ?>"><i class="fi-home"></i> <span>Accueil</span></a></li>
        <li>
            <a href="<?php echo site_url('CCategory'); ?>"><i class="fi-book"></i> <span>Cours</span></a>
            <ul class="nested vertical menu">
                <?php
                foreach (get_categories() as $cat) {
                    display_category($cat);
                }
                ?>
            </ul>
        </li>
        <?php 
        if(isset($_SESSION['token'])) {
        ?>
            <li><a href="<?php echo site_url('CCourse/create_form'); ?>"><i class="fi-plus"></i> <span>Créer un cours</span></a></li>
        <?php
        }
        ?>
    </ul>
</aside>
