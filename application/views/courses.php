<div class="row large-up-3">
<?php
foreach ($page['courses'] as $course) {
?>
    <div class="column">
        <div class="card">
            <div class="card-section">
                <h4>
                    <a href="<?php echo site_url('CCourse/view/' . $course->getId()); ?>">
                        <?php echo $course->getTitle(); ?>
                    </a>
                </h4>
                <p><?php echo $course->getDescription(); ?></p>
            </div>
        </div>
    </div>

<?php
}
?>
</div>
