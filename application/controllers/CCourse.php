<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CCourse extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        $data['page'] = array(
            'location' => 'cours'
        );
        $this->load->view('template/template', $data);
    }

    public function view($id)
    {
        $jcourses = $this->course->getCourseById($id);
        $course = $this->coursejson->parseCourse($jcourses);

        $data['page'] = array(
            'location'=> "course",
            'course' => $course
        );

        $this->load->view("template/template", $data);
    }

    public function create_form()
    {
        $dif_drop = [];
        $cat_drop = [];

        foreach (get_categories() as $cat) {
            $cat_drop[$cat->getId()] = $cat->getLabel();
            if (count($cat->getChildren()) > 0) {

                foreach ($cat->getChildren() as $child) {
                    $cat_drop[$child->getId()] = nbs(3) . $child->getLabel();
                }
            }
        }


        $form = array(
            'formTitle' => "Créer un cours",
            'formFields' => array(
                'title' => array(
                    'label' => 'Titre <small>required</small>',
                    'input' => array(
                        'id' => 'title',
                        'name' => 'title',
                        'placeholder' => 'Titre',
                        'required' => 'true'
                    )
                ),
                'description' => array(
                    'label' => 'Description <small>required</small>',
                    'textarea' => array(
                        'id' => 'description',
                        'name' => 'description',
                        'placeholder' => 'Description',
                        'required' => 'true'
                    )
                ),
                'content' => array(
                    'label' => 'Contenu <small>required</small>',
                    'textarea' => array(
                        'id' => 'content',
                        'name' => 'content',
                        'placeholder' => 'Contenu',
                        'required' => 'true'
                    )
                ),
                /* 'difficulty' => array(
                     'label' => 'Difficulté  <small>required</small>',
                     'dropdown' => array(
                         'id' => 'difficulty',
                         'name' => 'difficulty',
                         'attributes' => array('class' => 'form-control'),
                         'data' => $dif_drop,
                         'required' => 'true'
                     )
                 ),
                 /*'mainmedia' => array(
                     'label' => 'Password <small>required</small>',
                     'password' => array(
                         'id' => 'password',
                         'name' => 'password',
                         'placeholder' => 'Password',
                         'required' => 'true'
                     )
                 ),*/
                'category' => array(
                    'label' => 'Catégorie <small>required</small>',
                    'dropdown' => array(
                        'id' => 'category',
                        'name' => 'category',
                        'attributes' => array('class' => 'form-control'),
                        'data' => $cat_drop,
                        'required' => 'true'
                    )
                )
            ),
            'formUrl' => 'CCourse/create',
            'urlBase' => 'CCourse'
        );
        $data['page'] = array(
            'location' => 'template/form'
        );
        $data['page']['form'] = $form;
        $this->load->view('template/template', $data);
    }

    public function create()
    {
        $difficulty = 1;

        $this->form_validation->set_rules('title', 'Titre', 'trim|required');
        $this->form_validation->set_rules('content', 'Contenu', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="error">', '</small>');
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $description = $this->input->post('description');
        //$media = $this->input->post('mainmedia');
        //$difficulty = $this->input->post('difficulty');
        $category = $this->input->post('category');

        $array = array(
            "title" => "$title",
            "description" => "$description",
            "content" => "$content",
            "difficultyId" => $difficulty,
            //"mainMediaId" => $media,
            "categoryId" => $category
        );
        $jcourse = json_encode($array);
        $res = request("POST", '/course', $_SESSION['token'], $jcourse);

        redirect(base_url());
    }

    public function edit_form($id){


        $jcourses = $this->course->getCourseById($id);
        $course = $this->coursejson->parseCourse($jcourses);

        // $dif_drop = [];
        $cat_drop = [];

        $title = $course->getTitle();
        $content = $course->getContent();
        $description = $course->getDescription();
        //$media = "";
        //$difficulty = "";
        $category = 15;

        foreach (get_categories() as $cat) {
            $cat_drop[$cat->getId()] = $cat->getLabel();
            if (count($cat->getChildren()) > 0) {

                foreach ($cat->getChildren() as $child) {
                    $cat_drop[$child->getId()] = nbs(3) . $child->getLabel();
                }
            }
        }


        $form = array(
            'formTitle' => "Editer un cours",
            'formFields' => array(
                'title' => array(
                    'label' => 'Titre <small>required</small>',
                    'input' => array(
                        'id' => 'title',
                        'name' => 'title',
                        'value' => $title,
                        'placeholder' => 'Titre',
                        'required' => 'true'
                    )
                ),
                'description' => array(
                    'label' => 'Description <small>required</small>',
                    'textarea' => array(
                        'id' => 'description',
                        'name' => 'description',
                        'value' => $description,
                        'placeholder' => 'Description',
                        'required' => 'true'
                    )
                ),
                'content' => array(
                    'label' => 'Contenu <small>required</small>',
                    'textarea' => array(
                        'id' => 'content',
                        'name' => 'content',
                        'value' => $content,
                        'placeholder' => 'Contenu',
                        'required' => 'true'
                    )
                ),
                /* 'difficulty' => array(
                     'label' => 'Difficulté  <small>required</small>',
                     'dropdown' => array(
                         'id' => 'difficulty',
                         'name' => 'difficulty',
                         'attributes' => array('class' => 'form-control'),
                         'data' => $dif_drop,
                         'selected' => $difficulty,
                         'required' => 'true'
                     )
                 ),
                 /*'mainmedia' => array(
                     'label' => 'Password <small>required</small>',
                     'password' => array(
                         'id' => 'password',
                         'name' => 'password',
                         'placeholder' => 'Password',
                         'selected' => $media,
                         'required' => 'true'
                     )
                 ),*/
                'category' => array(
                    'label' => 'Catégorie <small>required</small>',
                    'dropdown' => array(
                        'id' => 'category',
                        'name' => 'category',
                        'attributes' => array('class' => 'form-control'),
                        'data' => $cat_drop,
                        'selected' => $category,
                        'required' => 'true'
                    )
                )
            ),
            'formUrl' => 'CCourse/edit/'.$id,
            'urlBase' => 'CCourse'
        );
        $data['page'] = array(
            'location' => 'template/form'
        );
        $data['page']['form'] = $form;
        $this->load->view('template/template', $data);
    }

    public function edit($id){
        $difficulty = 1;

        $this->form_validation->set_rules('title', 'Titre', 'trim|required');
        $this->form_validation->set_rules('content', 'Contenu', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="error">', '</small>');
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $description = $this->input->post('description');
        //$media = $this->input->post('mainmedia');
        //$difficulty = $this->input->post('difficulty');
        $category = $this->input->post('category');

        $array = array(
            "id" => $id,
            "title" => "$title",
            "description" => "$description",
            "content" => "$content",
            "difficultyId" => $difficulty,
            //"mainMediaId" => $media,
            "categoryId" => $category
        );
        $jcourse = json_encode($array);
        $res = request("PUT", '/course', $_SESSION['token'], $jcourse);

        redirect(base_url());
    }


}
