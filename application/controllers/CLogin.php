<?php

/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 22.02.2017
 * Time: 21:55
 */
class CLogin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function index()
    {
        $form = array(
            'formTitle' => 'Login',
            'formFields' => array(
                'username' => array(
                    'label' => 'Username <small>required</small>',
                    'input' => array(
                        'id' => 'username',
                        'name' => 'username',
                        'placeholder' => 'Username',
                        'required' => 'true'
                    )
                ),
                'password' => array(
                    'label' => 'Password <small>required</small>',
                    'password' => array(
                        'id' => 'password',
                        'name' => 'password',
                        'placeholder' => 'Password',
                        'required' => 'true'
                    )
                )
            ),
            'formUrl' => 'CLogin/login',
            'urlBase' => 'CLogin'
        );
        $data['page'] = array(
            'location' => 'login'
        );
        $data['page']['form'] = $form;
        $this->load->view('template/template', $data);
    }

    public function login()
    {

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="error">', '</small>');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $token = authenticate($username, $password);
        if (isset($token)) {
            $element = json_decode($token);
            $this->session->set_userdata(array('token' => $element->{'access_token'}));
            $this->session->mark_as_temp('token', $element->{'expires_in'});
            redirect(site_url());
        }
    }

    /*
     * disconnect the current user
     * empty and destroy the session
     * then redirect
     */
    public function disconnect()
    {
        $this->session->unset_userdata('token');
        session_destroy();
        redirect(site_url());
    }

    /*
     * Create and show the registration form
     */
    public function register_form()
    {
        $form = array(
            'formTitle' => "S'enregistrer",
            'formFields' => array(
                'username' => array(
                    'label' => 'Username <small>required</small>',
                    'input' => array(
                        'id' => 'username',
                        'name' => 'username',
                        'placeholder' => 'Username',
                        'required' => 'true'
                    )
                ),
                'firstname' => array(
                    'label' => 'First name ',
                    'input' => array(
                        'id' => 'firstname',
                        'name' => 'firstname',
                        'placeholder' => 'First name',
                        'required' => 'false'
                    )
                ),
                'lastname' => array(
                    'label' => 'Last name',
                    'input' => array(
                        'id' => 'lastname',
                        'name' => 'lastname',
                        'placeholder' => 'Last name',
                        'required' => 'false'
                    )
                ),
                'email' => array(
                    'label' => 'Email  <small>required</small>',
                    'input' => array(
                        'id' => 'email',
                        'name' => 'email',
                        'placeholder' => 'email',
                        'required' => 'true'
                    )
                ),
                'password' => array(
                    'label' => 'Password <small>required</small>',
                    'password' => array(
                        'id' => 'password',
                        'name' => 'password',
                        'placeholder' => 'Password',
                        'required' => 'true'
                    )
                ),
                'password2' => array(
                    'label' => 'Repeat password <small>required</small>',
                    'password' => array(
                        'id' => 'password2',
                        'name' => 'password2',
                        'placeholder' => 'Repeat password',
                        'required' => 'true'
                    )
                )
            ),
            'formUrl' => 'CLogin/register',
            'urlBase' => 'CLogin'
        );
        $data['page'] = array(
            'location' => 'login'
        );
        $data['page']['form'] = $form;
        $this->load->view('template/template', $data);
    }

    /*
     * Process the registration form
     */
    public function register()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|callback_unique_check');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password2]');
        $this->form_validation->set_rules('password2', 'Password', 'trim|required|callback_password_match');
        $this->form_validation->set_error_delimiters('<small class="error">', '</small>');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $password2 = $this->input->post('password2');
        $email = $this->input->post('email');
        $fname = $this->input->post('firstname');
        $lname = $this->input->post('lastname');

        $array = array(
            "firstName" => "$fname",
            "lastName" => "$lname",
            "username" => "$username",
            "email" => "$email",
            "password" => "$password"
            );
        $juser = json_encode($array);
        print_r($juser);
        return request("POST",'/user',null,$juser);
    }

    /*
     * Test if the passwords match
     */
    public function password_match()
    {
        $pwd = $this->input->post('password');
        $pwdconf = $this->input->post('password2');
        if ($pwd == $pwdconf) {
            return TRUE;
        } else {
            $this->form_validation->set_message("password_match", "passwords doesn't match");
            return FALSE;
        }
    }

    /*
    * Checks if the username already exists
    */

    public function unique_check()
    {
        $login = $this->input->post('username');
        $user = $this->user->getUserByUsername($login);

        if (isset($user)) {
            $this->form_validation->set_message('username_check', 'This user already exists"');
            return FALSE;
        } else {
            return true;
        }
    }
}