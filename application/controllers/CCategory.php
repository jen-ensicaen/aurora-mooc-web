<?php
class CCategory extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
	public function index(){
        $categories = get_categories();
		
        $data['page'] = array(
            'location' => 'categories',
			'categories' => $categories
        );

		$this->load->view("template/template",$data);
	}

    public function view($id){
        $jcourses = $this->course->getCoursesByCategory($id);
        $courses = $this->coursejson->parseCourses($jcourses);

        $data['page'] = array(
            'location'=> "courses",
            'courses' => $courses
        );

        $this->load->view("template/template",$data);
    }
}