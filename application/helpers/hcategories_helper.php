<?php

function get_categories() {
    $parser = new CategoryJson();

    $raw = Category::getCategories();
    $categories = $parser->parseCategories($raw);
    return $categories;
}

function display_category($category) {
    echo '<li>';
    echo '<a href="' . site_url('CCategory/view/' . $category->getId()) . '">' . $category->getLabel() . '</a>';

    if (count($category->getChildren()) > 0) {
        echo '<ul class="nested vertical menu">';

        foreach ($category->getChildren() as $child) {
            display_category($child);
        }

        echo '</ul>';
    }

    echo '</li>';
}

?>