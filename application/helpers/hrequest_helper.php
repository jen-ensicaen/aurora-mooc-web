<?php
/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 14.02.2017
 * Time: 15:27
 */
function authenticate($user_name, $user_pwd,$server_url="http://pr2a-candellier.ecole.ensicaen.fr:5000/api"){
    $url = $server_url."/token";
    $params = array('Username' => $user_name, 'Password' => $user_pwd);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT,1);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

    $result = curl_exec($ch);
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($http_status <= 200 || $http_status >= 226) {
        return $result;
    }
    return $result;
}

/*
 * Make a request to the API with the given URL and method
 * @param string $method the method to use (GET,POST,PUT,DELETE)
 * @param string $url the given short URL route
 * @param User $user The connected user which makes the request
 * @param string $server The base URL of the API server
 * @return mixed|false the result of the request, false if there's an error
 */

function request($method, $path_url, $token=null, $params=null, $server_url="http://pr2a-candellier.ecole.ensicaen.fr:5000/api") {
    $url = $server_url.$path_url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT,1);

    if(isset($token)) {
        $headers = array(
            $method . " " . $path_url . " HTTP/1.0",
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            //   "Cache-Control: no-cache",
            //  "Pragma: no-cache",
            //  "Content-length: ".strlen($xml_data),
            "Authorization: Bearer " . $token
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }
    if(isset($params)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params),
                "Authorization: Bearer " . $token)
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    }
    $result = curl_exec($ch);

    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $header = curl_getinfo($ch,CURLINFO_HEADER_OUT);
    curl_close($ch);
    if ($http_status >= 200 && $http_status <= 226) {
        return $result;
    }
    else{
        echo "ERREUR $http_status";
    }
    //return false;
}

/*
 * Post a file to the API
 * @param string $url the short URL route
 * @param User $user The connected user which makes the request
 * @param string $file The path of the file to send
 * @param string $server The base URL of the API server
 * @return mixed the result of the request
 */

function uploadRequest($server_url, $path_url, $token=null, $file) {

    $ch = curl_init();
    $cfile = curl_file_create($file['tmp_name'], $file['type'], $file['name']);
    $url = $server_url.$path_url;
    $data = array(
        'file' => $cfile
    );
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ($http_status >= 200 || $http_status <= 226) {
        return $result;
    }
    return false;
}