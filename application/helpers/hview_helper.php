<?php
/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 15.02.2017
 * Time: 08:54
 */
/*
 * Generates a form from a given array
 * @param mixed[] $form_array Array with the content of the form
 * @return string The HTML formatted resulting form
 */
function createForm($form_array) {
    $string_form = '';
    $cancel = 'Cancel';
    $form_open = '';
    $hidden = '';
    if (isset($form_array['multipart'])) {
        if ($form_array['multipart']) {
            $form_open = form_open_multipart($form_array['formUrl']);
            //$hidden = '<input type="hidden" name="MAX_FILE_SIZE" value="300000" />';
        }
    } else {
        $form_open = form_open($form_array['formUrl']);
    }
    $string_form .= '<fieldset><legend>' . $form_array['formTitle'] . '</legend>' . $form_open;
    foreach ($form_array['formFields'] as $field) {
        if (isset($field['input'])) {
            if (isset($field['label'])) {
                $string_form .= form_label($field['label'], $field['input']['id']) . nbs() . form_error($field['input']['id']);
            }
            $string_form .= form_input($field['input'], set_value($field['input']['id']));
        }
        if (isset($field['password'])) {
            if (isset($field['label'])) {
                $string_form .= form_label($field['label'], $field['password']['id']) . nbs() . form_error($field['password']['id']);
            }
            $string_form .= form_password($field['password'], set_value($field['password']['id']));
        }
        if (isset($field['dropdown'])) {
            $string_form .= form_label($field['label'], $field['dropdown']['id']) . nbs() . form_error($field['dropdown']['id']);
            if (isset($field['dropdown']['selected'])) {
                $string_form .= form_dropdown($field['dropdown']['id'], $field['dropdown']['data'], $field['dropdown']['selected'], $field['dropdown']['attributes']
                );
            } else {
                $string_form .= form_dropdown($field['dropdown']['id'], $field['dropdown']['data'], '', $field['dropdown']['attributes']
                );
            }
        }
        if (isset($field['textarea'])) {
            $string_form .= form_label($field['label'], $field['textarea']['id']) . nbs() . form_error($field['textarea']['id']) . form_textarea($field['textarea']);
        }
        if (isset($field['upload'])) {
            $string_form .= form_label($field['label'], $field['upload']['id']) . nbs() . form_error($field['upload']['id']) . form_upload($field['upload']);
        }
    }
    if (isset($form_array['btnCancel'])) {
        $cancel = $form_array['btnCancel'];
    }
    $string_form .= $hidden;
    $string_form .= '<div class="btn-cluster">' . form_input(array('type' => 'submit', 'class' => 'button'), 'Submit') .
        anchor(site_url($form_array['urlBase']), $cancel, 'class="alert button"') . '</div>' .
        form_close() . '</fieldset>';
    return $string_form;
}