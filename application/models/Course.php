<?php
Class Course extends CI_Model{
	private $id;
	private $title;
	private $description;
	private $content;
	private $creationDate;
	private $lastModifiedDate;
	private $creator;
	private $listMedia;
	private $listComments;
	private $category;
	private $difficulty;

    public function __construct() {
        parent::__construct();
    }

	function getCourseById($id){
	    $method = 'GET';
	    $uri = "/course/$id";
	    return request($method,$uri);
    }

    function getCoursesByCategory($id) {
        $method = 'GET';
	    $uri = "/course/category/$id";
	    return request($method,$uri);
    }

    function postCourse($course){
	    $title =  $course->getTitle();
	    $description = $course->getDescription();
	    $content = $course->getContent();

    }
	
	function getId(){
		return $this->id;
	}
	
	function getTitle(){
		return $this->title;
	}
	
	function getDescription(){
		return $this->description;
	}
	
	function getContent(){
		return $this->content;
	}
	
	function getCreationDate(){
		return $this->creationDate;
	}
	
	function getLastModifiedDate(){
		return $this->lastModifiedDate;
	}
	
	function getCreator(){
		return $this->creator;
	}
	
	function getListMedia(){
		return $this->listMedia;
	}
	
	function getListComments(){
		return $this->listComments;
	}
	
	function getCategory(){
		return $this->category;
	}
	
	function getDifficulty(){
		return $this->difficulty;
	}

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @param mixed $lastModifiedDate
     */
    public function setLastModifiedDate($lastModifiedDate)
    {
        $this->lastModifiedDate = $lastModifiedDate;
    }

    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param mixed $listMedia
     */
    public function setListMedia($listMedia)
    {
        $this->listMedia = $listMedia;
    }

    /**
     * @param mixed $listComments
     */
    public function setListComments($listComments)
    {
        $this->listComments = $listComments;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @param mixed $difficulty
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;
    }

}