<?php
class User extends CI_Model{
	private $id;
	private $firstname;
	private $lastname;
	private $username;
	private $email;
	private $password;
	private $creationDate;
	private $userType;
	private $profilePictureUrl;
	private $listCoursesCreated;
	private $listComments;

    public function __construct() {
        parent::__construct();
    }

    /**
     * User constructor.
     * @param $firstname
     * @param $lastname
     * @param $username
     * @param $email
     * @param $password
     * @param $userType
     */
    public function User($firstname, $lastname, $username, $email, $password)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
    }

    public function getUserByUsername($username){
        $url = "/user/byUsername/$username";
        $juser = request("GET", $url);
        if(isset($juser)){
            return $this->userJson->parseUser($juser);
        }else{
            return null;
        }
    }

    /**
     * @return mixed
     */
    public function getProfilePictureUrl()
    {
        return $this->profilePictureUrl;
    }

    /**
     * @param mixed $profilePictureUrl
     */
    public function setProfilePictureUrl($profilePictureUrl)
    {
        $this->profilePictureUrl = $profilePictureUrl;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getListCoursesCreated()
    {
        return $this->listCoursesCreated;
    }

    /**
     * @param mixed $listCoursesCreated
     */
    public function setListCoursesCreated($listCoursesCreated)
    {
        $this->listCoursesCreated = $listCoursesCreated;
    }

    /**
     * @return mixed
     */
    public function getListComments()
    {
        return $this->listComments;
    }

    /**
     * @param mixed $listComments
     */
    public function setListComments($listComments)
    {
        $this->listComments = $listComments;
    }
}