<?php
Class UserType extends CI_Model{
	private $id;
	private $label;
	private $listUsers;

    public function __construct() {
        parent::__construct();
    }

    /**
     * UserType constructor.
     * @param $label
     */
    public function UserType($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getListUsers()
    {
        return $this->listUsers;
    }

    /**
     * @param mixed $listUsers
     */
    public function setListUsers($listUsers)
    {
        $this->listUsers = $listUsers;
    }


}