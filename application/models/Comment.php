<?php
Class Comment extends CI_Model{
	private $id;
	private $message;
	private $date;
	private $user;
	private $course;

    public function __construct() {
        parent::__construct();
    }
	
	function Comment($message,$date,$user,$course){
		$this->message=$message;
		$this->date=$date;
		$this->user=$user;
		$this->course=$course;
	}
	
	function getId(){
		return $this->id;
	}
	
	function getMessage(){
		return $this->message;
	}
	
	function getDate(){
		return $this->date;
	}
	
	function getUser(){
		return $this->user;
	}
	
	function getCourse(){
		return $this->course;
	}
}