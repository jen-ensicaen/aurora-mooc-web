<?php
class Category extends CI_Model{
	private $id;
	private $label;
    private $children;

    public function __construct() {
        parent::__construct();
        $children = array();
    }

    static function getCategories(){
        $method = 'GET';
        $uri = "/course/category/";
        return request($method, $uri);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
	
    public function getChildren()
    {
        return $this->children;
    }

    public function setChildren($children)
    {
        $this->children = $children;
    }

}