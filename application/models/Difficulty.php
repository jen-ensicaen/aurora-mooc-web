<?php
Class Difficulty extends CI_Model{
	private $id;
	private $label;

    public function __construct() {
        parent::__construct();
    }



    /**
     * Difficulty constructor.
     * @param $label
     */
    public function Difficulty($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }




}