<?php

/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 15.02.2017
 * Time: 11:33
 */
class CategoryJson
{
    public function parseCategories($elements){
        if (!empty($elements)) {
            $CI = &get_instance();
            $jcategories = json_decode($elements);
            $categories = self::doParse($jcategories);
            return $categories;
        }
    }

    private function doParse($jcategories) {
        $categories = array();
        $i = 0;

        foreach ($jcategories as $jcat){
            $cat = new Category();
            $cat->setId($jcat->{'id'});
            $cat->setLabel($jcat->{'label'});
            $cat->setChildren(self::doParse($jcat->{'children'}));
            $categories[$i] = $cat;
            $i = $i +1;
        }

        return $categories;
    }
}