<?php

/**
 * Created by PhpStorm.
 * User: Snow
 * Date: 23.02.2017
 * Time: 10:15
 */
class UserJson
{
    public function parseUser($element){
        $user = new User();
        $userType = new UserType();
        if (!empty($element)) {
            $juser = json_decode($element);
            $user->setId($element->{'id'});
            $user->setUsername($element->{'username'});
            $user->setFirstname($element->{'firstName'});
            $user->setLastname($element->{'lastName'});
            $user->setEmail($element->{'email'});
            $user->setCreationDate($element->{'creationDate'});
            $userType->setId($element->{'type'}->{'id'});
            $userType->setLabel($element->{'type'}->{'label'});
            $user->setUserType($userType);
            $user->setProfilePictureUrl($element->{'profilePictureUrl'});
            }
            return $user;
        }
}