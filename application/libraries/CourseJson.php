<?php

class CourseJson
{
    public function parseCourses($elements){
        if (!empty($elements)) {
            $CI = &get_instance();
            $jcourses = json_decode($elements);
            $courses = array();

            foreach ($jcourses as $jcourse) {
                $course = new Course();
                $course->setId($jcourse->{'id'});
                $course->setTitle($jcourse->{'title'});
                $course->setDescription($jcourse->{'description'});
                $course->setContent($jcourse->{'content'});
                $course->setCreationDate($jcourse->{'creationDate'});
                $course->setLastModifiedDate($jcourse->{'lastModifiedDate'});
                $courses[] = $course;
            }
            
            return $courses;
        }
    }

    public function parseCourse($element){
        $CI = &get_instance();
        $jcourse = json_decode($element);
        
        $course = new Course();
        $course->setId($jcourse->{'id'});
        $course->setTitle($jcourse->{'title'});
        $course->setDescription($jcourse->{'description'});
        $course->setContent($jcourse->{'content'});
        $course->setCreationDate($jcourse->{'creationDate'});
        $course->setLastModifiedDate($jcourse->{'lastModifiedDate'});
        
        return $course;
    }

}